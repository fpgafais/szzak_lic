#include <iostream>
#include <cmath>

__global__ void kernel(int n, float* x, float* y, float* z) {
        int index = blockIdx.x * blockDim.x + threadIdx.x;
        int stride = blockDim.x * gridDim.x;
        for (int i = index; i < n; i += stride)
                z[i] = x[i] + y[i];
}

int main() {
        int N = 1 << 2;
        float* x, * y, * z;
        cudaMallocManaged(&x, N * sizeof(float));
        cudaMallocManaged(&y, N * sizeof(float));
        cudaMallocManaged(&z, N * sizeof(float));

        for (int i = 0; i < N; i++) {
                x[i] = 1.0f;
                y[i] = 2.0f;
        }

        int blockSize = 256;
        int numBlocks = (N + blockSize - 1) / blockSize;
        kernel<<<numBlocks, blockSize>>>(N, x, y, z);
        cudaDeviceSynchronize();

        float maxError = 0.0f;
        for (int i = 0; i < N; i++)
                maxError = fmax(maxError, fabs(z[i] - 3.0f));
        std::cout << "Max error: " << maxError << std::endl;

        cudaFree(x);
        cudaFree(y);
        cudaFree(z);

        return 0;
}


