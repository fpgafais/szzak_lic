cmake_minimum_required(VERSION 3.17)
project(cuda_example CUDA)

set(CMAKE_CUDA_STANDARD 14)

add_executable(cuda_example main.cu)

set_target_properties(
        cuda_example
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)