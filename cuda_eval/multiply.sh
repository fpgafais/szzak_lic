#!/usr/bin/env bash

# Script to multiply given input in 'resources' directory
# Can multiply matrices with Julia, C++ and CUDA
# usage: multiply.sh [ jul | cpu | gpu ]

. scripts/bash/init_functions.sh
CODE=0

if [[ $# -le 0 ]]; then
  echo "You need to pass at least one argument [ jul | cpu | gpu ]"
  exit 1
fi

printf "Matrix multiply "
start

if [[ "$*" =~ "jul" ]]; then
  printf "Multiply with Julia"
  if ! julia_multiply; then
    CODE=1
    fail
  fi
  ok
fi

if [[ "$*" =~ "cpu" ]]; then
  printf "Multiply with C++ on CPU \nTime = "
  if ! ./bin/cpu_main <resources/input.txt >resources/cpu_output.txt; then
    CODE=1
    printf "CPU multiply "
    fail
  fi
  printf "CPU multiply "
  ok
fi

if [[ "$*" =~ "gpu" ]]; then
  printf "Multiply with CUDA on GPU\nTime = "
  if ! ./bin/cuda_main <resources/input.txt >resources/gpu_output.txt; then
    CODE=1
    printf "GPU multiply "
    fail
  fi
  printf "GPU multiply "
  ok
fi

check_exit "Multiply" $CODE
