#!/usr/bin/env bash

. scripts/bash/init_functions.sh
CODE=0

printf "Tests "
start

echo "Test for CPU implementation "
if ! ./bin/cpu_tests; then
  CODE=1
  printf "CPU test "
  fail
else
  printf "CPU test "
  ok
fi

echo "Test for GPU implementation "
if ! ./bin/gpu_tests; then
  CODE=1
  printf "GPU test "
  fail
else
  printf "GPU test "
  ok
fi

check_exit "Tests" $CODE
