#!/usr/bin/env bash
. scripts/bash/init_functions.sh

if [[ "$*" =~ "clean" ]]; then
  printf "Running clean "
  if ! ./clean.sh; then
    check_exit "Build" 1
  fi
fi

if [[ "$*" =~ "all" ]]; then
  set -- "test check"
fi

if ! (cmake -S . -B build && cmake --build build); then
  check_exit "Build" 1
fi

if [[ "$*" =~ "test" ]]; then
  printf "Running test "
  if ! ./test.sh; then
    check_exit "Build" 1
  fi
fi

if [[ "$*" =~ "check" ]]; then
  printf "Running check "
  if ! ./check.sh; then
    check_exit "Build" 1
  fi
fi

check_exit "Build" 0
