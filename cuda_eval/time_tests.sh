#!/usr/bin/env bash

. scripts/bash/init_functions.sh

for size in 4 16 64 128 256 512 1024; do
  printf "Checking size %s " $size
  start

  ./generateInput.sh $size $size $size &&
    ./multiply.sh gpu cpu &&
    ./validate.sh gpu cpu

  check_exit "Check" $?
done
