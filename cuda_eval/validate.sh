#!/usr/bin/env bash

. scripts/bash/init_functions.sh
CODE=0

if [[ $# -le 0 ]]; then
  echo "You need to pass at least one argument"
  check_exit "CHECK" 1
fi

printf "Result validation "
start

if ! julia_multiply; then
  echo "Cannot create test data"
  check_exit "CHECK" 1
fi

if [[ "$*" =~ "cpu" ]]; then
  printf "Comparison for CPU results "
  if ! diff -Bb "resources/julia_output.txt" "resources/cpu_output.txt"; then
    CODE=1
    fail
  fi
  ok
fi

if [[ "$*" =~ "gpu" ]]; then
  printf "Comparison for GPU results "
  if ! diff -Bb "resources/julia_output.txt" "resources/gpu_output.txt"; then
    CODE=1
    fail
  fi
  ok
fi

check_exit "Validation" $CODE
