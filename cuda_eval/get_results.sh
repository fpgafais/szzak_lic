#!/bin/bash

. scripts/bash/init_functions.sh

printf "DimM, DimN, DimW, timeUnit, createMatrixATime, createMatrixBTime, calculationTime\n" >results.txt
for size in 4 16 64 128 256 512 1024 2048; do
  printf "Checking size %s " $size
  ./bin/cuda_main_measure <testRes/dataSets/$size-$size-$size.txt >testRes/dataSetsGPUOut/$size-$size-$size.txt 2>>results.txt &&
    diff -Bb "testRes/dataSetsOut/$size-$size-$size.txt" "testRes/dataSetsGPUOut/$size-$size-$size.txt"
  check_exit "Check" $?
done
printf "memcpy\n" >>results.txt
for size in 4 16 64 128 256 512 1024 2048; do
  printf "Checking size %s " $size
  ./bin/cuda_main_measure_memcpy <testRes/dataSets/$size-$size-$size.txt >testRes/dataSetsGPUMemcpyOut/$size-$size-$size.txt 2>>results.txt &&
    diff -Bb "testRes/dataSetsOut/$size-$size-$size.txt" "testRes/dataSetsGPUMemcpyOut/$size-$size-$size.txt"
  check_exit "Check" $?
done
