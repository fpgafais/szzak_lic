#!/usr/bin/env bash

# Generates random input matrices A[m x n] and B[n x w]
# Usage: ./generate_input.sh [M] [N] [W]
. scripts/bash/init_functions.sh
CODE=0

printf "Generating input "
start

if [[ $# -ne 3 ]]; then
  echo "You need to pass three arguments"
  exit 1
fi

if ! [[ -e resources ]]; then
  mkdir resources
fi

if ! julia scripts/julia/generator.jl "$@" >resources/input.txt; then
  CODE=1
fi

check_exit "Generate" ${CODE}
