#!/usr/bin/julia

# generator for two 2-dimension integer matrices
# call it like 'julia generator.jl [M] [N] [W]' where M,N,W are dimension length
# it will generate two matrices : A [MxN] and B [NxW]

const MAX = 30
const MIN = 0

m, n, w = ARGS

println(m, " ", n, " ", w)

for i in 1:parse(Int64, m)
    for j in 1:parse(Int64, n)
        print(rand(MIN:MAX), " ")
    end
    println()
end

for i in 1:parse(Int64, n)
    for j in 1:parse(Int64, w)
        print(rand(MIN:MAX), " ")
    end
    println()
end