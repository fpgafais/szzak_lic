#!/usr/bin/julia

dimensions = [parse(Int, x) for x in split(readline())]
A = zeros(Int64, dimensions[1], dimensions[2])
B = zeros(Int64, dimensions[2], dimensions[3])

for i in 1:dimensions[1]
    line = split(readline())
    for j in 1:dimensions[2]
        A[i, j] = parse(Int, line[j])
    end
end

for i in 1:dimensions[2]
    line = split(readline())
    for j in 1:dimensions[3]
        B[i, j] = parse(Int, line[j])
    end
end

C = A * B
for i in 1:dimensions[1]
    for j in 1:dimensions[3]
        print(C[i, j], " ")
    end
    println()
end
