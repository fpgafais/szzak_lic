#!/usr/bin/env bash

# tries to remove given directories
function try_remove() {
  if [[ $# -le 0 ]]; then
    echo "You need to pass at least one argument"
    return 1
  fi

  for dir in "$@"; do
    if [[ -e $dir ]]; then
      rm -rf "$dir"
      echo "$dir removed"
    fi
  done
}

# multiplies matrices in input.txt and saves to julia_output.txt
function julia_multiply() {
  julia scripts/julia/matrix-multiply.jl <resources/input.txt >resources/julia_output.txt
  return $?
}

# checks if exit code is equal to 0 and prompts OK or FAIL
function check_exit() {
  printf "%s " "$1"
  if [[ "$2" == "0" ]]; then
    ok
  else
    fail
    exit $(($2))
  fi
}

function ok() {
  echo -e "\e[32mOK\e[0m\n"
}

function fail() {
  echo -e "\e[31mFAIL\e[0m\n"
}

function start() {
  echo -e "\e[33mSTART\e[0m\n"
}
