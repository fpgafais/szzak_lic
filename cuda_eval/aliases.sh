#!/usr/bin/env bash

echo "SETTING ALIASES"

alias build=./build.sh
alias test=./test.sh
alias check=./check.sh
alias multiply=./multiply.sh
alias validate=./validate.sh
alias clean=./clean.sh

echo "OK"
