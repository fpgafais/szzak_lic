#!/usr/bin/env bash

echo "Cleaning"

. scripts/bash/init_functions.sh

try_remove resources build cmake-build-debug bin

echo "Cleaning OK"
