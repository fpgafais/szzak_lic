Author: Szymon Zak  

1. Required software

    - C++ language compiler (g++ preferred)
    - CMake (with Make)
    - Boost
    - CUDA 10.2

2. Compilation

    Execute following command: 
    `cmake -S . -B build && cmake --build build`

3. How to run

    Within compilation "bin" directory should be created.  
    Enter that directory with `cd bin`.  
    It should contain following files:

    - cpu_main - program to multiply two given matrices, standard CPU implementation
    - cuda_main - program to multiply two given matrices, CUDA accelerated implementation
    - cpu_tests - tests for CPU implementation
    - gpu_tests - tests for CUDA implementation

    Each of these files is executable program that may be run.

4. Description

    Both two main programs accept the same input and produces the same output.  
    However, they differ in way how they compute.  
    Input:

        m, n, w - integer numbers that describes matrices length
        A[m x n] - matrix with m rows and n columns
        B[n x w] - matrix with n rows and w columns

    Output:

        C[m x w] - computer matrix with m rows and w columns

    Input format:

        m n w
        first row of matrix A : n numbers, separated by space
        second row...
        ...
        m-th row of matrix A...
        first row of matrix B : w numbers, separated by space
        second...
        ...
        n-th row of matrix B...

    Example input (A[2 x 2], B[2 x 3]):
    
        2 2 3
        2 2
        2 2
        1 1 1
        1 1 1

    Example output ( C[2 x 3]:

        4 4 4
        4 4 4

    Also, time duration of computing will be printed to standard error output.

5. Additional scripts  
    #TODO