# Basic cmake file for top-level-project
cmake_minimum_required(VERSION 3.13)
project(CUDA_EVAL
        DESCRIPTION "CUDA and standard CPU-based C++ comparison project"
        LANGUAGES CXX CUDA)

# dependencies
find_package(Boost COMPONENTS unit_test_framework REQUIRED)

# test config
if (CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME)
    include(CTest)
endif ()

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)

add_subdirectory(src)