#!/usr/bin/env bash

# script that checks both CPU and GPU implementation

SIZE=(1024 1024 1024)

. scripts/bash/init_functions.sh

printf "Checking flow "
start

./generateInput.sh "${SIZE[@]}" &&
  ./multiply.sh gpu cpu &&
  ./validate.sh gpu cpu

check_exit "Flow check" $?
