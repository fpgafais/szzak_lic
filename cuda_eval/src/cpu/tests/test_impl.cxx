#include <boost/test/unit_test.hpp>
#include <commons.hxx>
#include <test_commons.hxx>
#include <multiply.hxx>

void cpuTest(const std::string &testSrc, int m, int n, int w) {
        //given
        auto matrixA = createMatrixFromFile(testSrc + "a.txt", m, n);
        auto matrixB = createMatrixFromFile(testSrc + "b.txt", n, w);
        auto expected = createMatrixFromFile(testSrc + "c.txt", m, w);

        //when
        auto actual = new int[m * w];
        cpuMultiply(matrixA, matrixB, actual, m, n, w);

        //then
        BOOST_CHECK(matrixEquals(expected, actual, m, w));

        //after
        delete[] matrixA;
        delete[] matrixB;
        delete[] expected;
        delete[] actual;
}

void cpuTest2D(const std::string &testSrc, int m, int n, int w) {
        //given
        auto matrixA = createMatrix2DFromFile(testSrc + "a.txt", m, n);
        auto matrixB = createMatrix2DFromFile(testSrc + "b.txt", n, w);
        auto expected = createMatrix2DFromFile(testSrc + "c.txt", m, w);

        //when
        auto actual = createMatrix2D(m, w);
        cpuMultiply2D(matrixA, matrixB, actual, m, n, w);

        //then
        BOOST_CHECK(matrixEquals2D(expected, actual, m, w));

        //after
        deleteMatrix2D(matrixA, m);
        deleteMatrix2D(matrixB, n);
        deleteMatrix2D(expected, m);
        deleteMatrix2D(actual, m);
}