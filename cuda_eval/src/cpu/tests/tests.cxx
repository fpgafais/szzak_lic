#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE CpuTests

#include <boost/test/unit_test.hpp>
#include "test_impl.hxx"

using source = const std::string;

// 1D tests

BOOST_AUTO_TEST_CASE(cpu_1x1) {
        constexpr int m = 1, n = 1, w = 1;
        source testSrc = "testRes/1x1/";
        cpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu_1x5_5x3) {
        constexpr int m = 1, n = 5, w = 3;
        source testSrc = "testRes/1x5_5x3/";
        cpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu_2x2) {
        constexpr int m = 2, n = 2, w = 2;
        source testSrc = "testRes/2x2/";
        cpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu_2x3_3x2) {
        constexpr int m = 2, n = 3, w = 2;
        source testSrc = "testRes/2x3_3x2/";
        cpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu_3x2_2x3) {
        constexpr int m = 3, n = 2, w = 3;
        source testSrc = "testRes/3x2_2x3/";
        cpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu_4x1_1x3) {
        constexpr int m = 4, n = 1, w = 3;
        source testSrc = "testRes/4x1_1x3/";
        cpuTest(testSrc, m, n, w);
}

// 2D tests

BOOST_AUTO_TEST_CASE(cpu2D_1x1) {
        constexpr int m = 1, n = 1, w = 1;
        source testSrc = "testRes/1x1/";
        cpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu2D_1x5_5x3) {
        constexpr int m = 1, n = 5, w = 3;
        source testSrc = "testRes/1x5_5x3/";
        cpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu2D_2x2) {
        constexpr int m = 2, n = 2, w = 2;
        source testSrc = "testRes/2x2/";
        cpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu2D_2x3_3x2) {
        constexpr int m = 2, n = 3, w = 2;
        source testSrc = "testRes/2x3_3x2/";
        cpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu2D_3x2_2x3) {
        constexpr int m = 3, n = 2, w = 3;
        source testSrc = "testRes/3x2_2x3/";
        cpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(cpu2D_4x1_1x3) {
        constexpr int m = 4, n = 1, w = 3;
        source testSrc = "testRes/4x1_1x3/";
        cpuTest2D(testSrc, m, n, w);
}