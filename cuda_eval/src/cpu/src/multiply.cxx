#include "multiply.hxx"

void cpuMultiply(value_ptr matrixA, value_ptr matrixB, value_ptr outputMatrix,
                 int m, int n, int w) {
        for (auto i = 0; i < m; i++) {
                const int inRow = i * n;
                const int outRow = i * w;
                for (auto j = 0; j < w; j++) {
                        int sum = 0;
                        for (auto k = 0; k < n; k++) {
                                sum += matrixA[inRow + k] * matrixB[k * w + j];
                        }
                        outputMatrix[outRow + j] = sum;
                }
        }
}

void cpuMultiply2D(value_ptr2D matrixA, value_ptr2D matrixB, value_ptr2D outputMatrix,
                   int m, int n, int w) {
        for (auto i = 0; i < m; i++) {
                for (auto j = 0; j < w; j++) {
                        int sum = 0;
                        for (auto k = 0; k < n; k++) {
                                sum += matrixA[i][k] * matrixB[k][j];
                        }
                        outputMatrix[i][j] = sum;
                }
        }
}
