#ifndef CUDA_EVAL_MULTIPLY_HXX
#define CUDA_EVAL_MULTIPLY_HXX

#include "types.hxx"

void cpuMultiply(value_ptr, value_ptr, value_ptr, int, int, int);

void cpuMultiply2D(value_ptr2D, value_ptr2D, value_ptr2D, int, int, int);

#endif //CUDA_EVAL_MULTIPLY_HXX
