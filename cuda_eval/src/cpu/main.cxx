#include <iostream>
#include <commons.hxx>
#include "multiply.hxx"

// matrix multiplication on CPU;
// C[n,w] = A[m x n] * B[n x w]

void run(int, int, int);

void run2D(int, int, int);

int main() {
        int m, n, w;
        std::cin >> m >> n >> w;
        run(m, n, w);
}

void run(int m, int n, int w) {
        auto matrixA = new value_t[m * n];
        auto matrixB = new value_t[n * w];
        auto matrixC = new value_t[m * w];
        fillMatrixFromCin(matrixA, m, n);
        fillMatrixFromCin(matrixB, n, w);

        auto start = getTime();
        cpuMultiply(matrixA, matrixB, matrixC, m, n, w);
        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << time_unit_s << std::endl;
        printMatrix(matrixC, m, w);

        delete[] matrixA;
        delete[] matrixB;
        delete[] matrixC;
}

void run2D(int m, int n, int w) {
        auto matrixA = createMatrix2D(m, n);
        auto matrixB = createMatrix2D(n, w);
        auto matrixC = createMatrix2D(m, w);
        fillMatrix2DFromCin(matrixA, m, n);
        fillMatrix2DFromCin(matrixB, n, w);

        auto start = getTime();
        cpuMultiply2D(matrixA, matrixB, matrixC, m, n, w);
        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << time_unit_s << std::endl;
        printMatrix2D(matrixC, m, w);

        deleteMatrix2D(matrixA, m);
        deleteMatrix2D(matrixB, n);
        deleteMatrix2D(matrixC, m);
}