#include <iostream>
#include "commons.hxx"
#include "commons.cuh"
#include "multiply.cuh"

#define BLOCK_SIZE 16

// matrix multiplication on GPU;
// C[n,w] = A[m x n] * B[n x w]

void run(int, int, int);

void run2D(int, int, int);

int main() {
        int m, n, w;
        std::cin >> m >> n >> w;
        run(m, n, w);
}

void run(int m, int n, int w) {
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

        auto matrixA = createCudaMatrix(m, n);
        auto matrixB = createCudaMatrix(n, w);
        auto matrixC = createCudaMatrix(m, w);

        fillMatrixFromCin(matrixA, m, n);
        fillMatrixFromCin(matrixB, n, w);

        auto start = getTime();
        gpuMultiply<<<dimGrid, dimBlock>>>(matrixA, matrixB, matrixC, m, n, w);
        cudaDeviceSynchronize();
        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << time_unit_s << std::endl;
        printMatrix(matrixC, m, w);

        deleteCudaMatrix(matrixA);
        deleteCudaMatrix(matrixB);
        deleteCudaMatrix(matrixC);
}

void run2D(int m, int n, int w) {
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

        auto matrixA = createCudaMatrix2D(m, n);
        auto matrixB = createCudaMatrix2D(n, w);
        auto matrixC = createCudaMatrix2D(m, w);
        fillMatrix2DFromCin(matrixA, m, n);
        fillMatrix2DFromCin(matrixB, n, w);

        // cudaMemCpy

        auto start = getTime();
        gpuMultiply2D<<<dimGrid, dimBlock>>>(matrixA, matrixB, matrixC, m, n, w);
        cudaDeviceSynchronize();
        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << time_unit_s << std::endl;
        printMatrix2D(matrixC, m, w);

        deleteCudaMatrix2D(matrixA, m);
        deleteCudaMatrix2D(matrixB, n);
        deleteCudaMatrix2D(matrixC, m);
}