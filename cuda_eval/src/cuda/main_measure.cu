#include <iostream>
#include "commons.hxx"
#include "commons.cuh"
#include "multiply.cuh"

#define BLOCK_SIZE 16

// matrix multiplication on GPU;
// C[n,w] = A[m x n] * B[n x w]

void runWithChrono(int, int, int);

int main() {
        int m, n, w;
        std::cin >> m >> n >> w;
        runWithChrono(m, n, w);
}

void runWithChrono(int m, int n, int w) {
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);


        std::cerr << m << ", " << n << ", " << w << "," << time_unit_s << ", ";

        auto stdMatrixA = new value_t[m * n];
        fillMatrixFromCin(stdMatrixA, m, n);
        auto start = getTime();

        auto matrixA = createCudaMatrix(m, n);
        for (auto i = 0; i < m * n; i++) {
                matrixA[i] = stdMatrixA[i];
        }

        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << ", ";

        auto stdMatrixB = new value_t[m * n];
        fillMatrixFromCin(stdMatrixB, n, w);

        start = getTime();

        auto matrixB = createCudaMatrix(n, w);
        for (auto i = 0; i < m * n; i++) {
                matrixB[i] = stdMatrixB[i];
        }

        end = getTime();

        delete[] stdMatrixB;
        delete[] stdMatrixA;

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << ", ";

        auto matrixC = createCudaMatrix(m, w);

        start = getTime();
        gpuMultiply<<<dimGrid, dimBlock>>>(matrixA, matrixB, matrixC, m, n, w);
        cudaDeviceSynchronize();
        end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << std::endl;
        printMatrix(matrixC, m, w);

        deleteCudaMatrix(matrixA);
        deleteCudaMatrix(matrixB);
        deleteCudaMatrix(matrixC);
}