#include <iostream>
#include "commons.hxx"
#include "commons.cuh"
#include "multiply.cuh"

#define BLOCK_SIZE 16

// matrix multiplication on GPU;
// C[n,w] = A[m x n] * B[n x w]

void runWithChronoMemCpy(int, int, int);

int main() {
        int m, n, w;
        std::cin >> m >> n >> w;
        runWithChronoMemCpy(m, n, w);
}

void runWithChronoMemCpy(int m, int n, int w) {
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);


        std::cerr << m << ", " << n << ", " << w << "," << time_unit_s << ", ";

        auto stdMatrixA = new value_t[m * n];
        fillMatrixFromCin(stdMatrixA, m, n);

        auto start = getTime();

        auto matrixA = createCudaMatrix(m, n);
        cudaMemcpy(matrixA, stdMatrixA, m * n * sizeof(value_t), cudaMemcpyHostToDevice);

        auto end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << ", ";
        delete[] stdMatrixA;

        auto stdMatrixB = new value_t[m * n];
        fillMatrixFromCin(stdMatrixB, n, w);

        start = getTime();

        auto matrixB = createCudaMatrix(n, w);
        cudaMemcpy(matrixB, stdMatrixB, m * n * sizeof(value_t), cudaMemcpyHostToDevice);

        end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << ", ";
        delete[] stdMatrixB;

        auto stdMatrixS = new value_t[m * n];
        fillMatrixFromCin(stdMatrixA, m, n);

        start = getTime();

        auto matrixS = createCudaMatrix(m, n);
        cudaMemcpy(matrixS, stdMatrixS, m * n * sizeof(value_t), cudaMemcpyHostToDevice);

        end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << ", ";
        delete[] stdMatrixS;

        auto matrixC = createCudaMatrix(m, w);

        start = getTime();
        gpuMultiply<<<dimGrid, dimBlock>>>(matrixA, matrixB, matrixC, m, n, w);
        cudaDeviceSynchronize();
        end = getTime();

        std::cerr << std::chrono::duration_cast<time_unit>(end - start).count() << std::endl;
        printMatrix(matrixC, m, w);

        deleteCudaMatrix(matrixA);
        deleteCudaMatrix(matrixB);
        deleteCudaMatrix(matrixC);
}