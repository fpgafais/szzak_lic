#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE GpuTests

#include <boost/test/unit_test.hpp>
#include "test_impl.cuh"

using source = const std::string;

BOOST_AUTO_TEST_CASE(gpu_1x1) {
        constexpr int m = 1, n = 1, w = 1;
        source testSrc = "testRes/1x1/";
        gpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu_1x5_5x3) {
        constexpr int m = 1, n = 5, w = 3;
        source testSrc = "testRes/1x5_5x3/";
        gpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu_2x2) {
        constexpr int m = 2, n = 2, w = 2;
        source testSrc = "testRes/2x2/";
        gpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu_2x3_3x2) {
        constexpr int m = 2, n = 3, w = 2;
        source testSrc = "testRes/2x3_3x2/";
        gpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu_3x2_2x3) {
        constexpr int m = 3, n = 2, w = 3;
        source testSrc = "testRes/3x2_2x3/";
        gpuTest(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu_4x1_1x3) {
        constexpr int m = 4, n = 1, w = 3;
        source testSrc = "testRes/4x1_1x3/";
        gpuTest(testSrc, m, n, w);
}

// 2D tests

BOOST_AUTO_TEST_CASE(gpu2D_1x1) {
        constexpr int m = 1, n = 1, w = 1;
        source testSrc = "testRes/1x1/";
        gpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu2D_1x5_5x1) {
        constexpr int m = 1, n = 5, w = 3;
        source testSrc = "testRes/1x5_5x3/";
        gpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu2D_2x2) {
        constexpr int m = 2, n = 2, w = 2;
        source testSrc = "testRes/2x2/";
        gpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu2D_3x2_2x3) {
        constexpr int m = 3, n = 2, w = 3;
        source testSrc = "testRes/3x2_2x3/";
        gpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu2D_2x3_3x2) {
        constexpr int m = 2, n = 3, w = 2;
        source testSrc = "testRes/2x3_3x2/";
        gpuTest2D(testSrc, m, n, w);
}

BOOST_AUTO_TEST_CASE(gpu2D_1x5_5x3) {
        constexpr int m = 1, n = 5, w = 3;
        source testSrc = "testRes/1x5_5x3/";
        gpuTest2D(testSrc, m, n, w);
}