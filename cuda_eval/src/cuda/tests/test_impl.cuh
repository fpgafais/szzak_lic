#ifndef CUDA_EVAL_TEST_IMPL_CUH
#define CUDA_EVAL_TEST_IMPL_CUH

#define BLOCK_SIZE 16

#include <string>

void gpuTest(const std::string &, const int, const int, const int);

void gpuTest2D(const std::string &, const int, const int, const int);

#endif //CUDA_EVAL_TEST_IMPL_CUH
