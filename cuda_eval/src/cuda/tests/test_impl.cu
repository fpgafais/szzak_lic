#include <boost/test/unit_test.hpp>
#include "multiply.cuh"
#include "commons.cuh"
#include "test_commons.hxx"
#include "test_impl.cuh"

void gpuTest(const std::string &testSrc, const int m, const int n, const int w) {
        //given
        auto matrixA = createCudaMatrixFromFile(testSrc + "a.txt", m, n);
        auto matrixB = createCudaMatrixFromFile(testSrc + "b.txt", n, w);
        auto expected = createCudaMatrixFromFile(testSrc + "c.txt", m, w);

        //when
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
        auto actual = createCudaMatrix(m, w);
        gpuMultiply<<<dimGrid, dimBlock>>>(matrixA, matrixB, actual, m, n, w);
        cudaDeviceSynchronize();

        //then
        BOOST_CHECK(matrixEquals(expected, actual, m, w));

        //after
        deleteCudaMatrix(matrixA);
        deleteCudaMatrix(matrixB);
        deleteCudaMatrix(expected);
        deleteCudaMatrix(actual);
}

void gpuTest2D(const std::string &testSrc, const int m, const int n, const int w) {
        //given
        auto matrixA = createCudaMatrix2DFromFile(testSrc + "a.txt", m, n);
        auto matrixB = createCudaMatrix2DFromFile(testSrc + "b.txt", n, w);
        auto expected = createCudaMatrix2DFromFile(testSrc + "c.txt", m, w);

        //when
        const unsigned int grid_rows = (m + BLOCK_SIZE - 1) / BLOCK_SIZE;
        const unsigned int grid_cols = (w + BLOCK_SIZE - 1) / BLOCK_SIZE;
        dim3 dimGrid(grid_cols, grid_rows);
        dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);
        auto actual = createCudaMatrix2D(m, w);
        gpuMultiply2D<<<dimGrid, dimBlock>>>(matrixA, matrixB, actual, m, n, w);
        cudaDeviceSynchronize();

        //then
        BOOST_CHECK(matrixEquals2D(expected, actual, m, w));

        //after
        deleteCudaMatrix2D(matrixA, m);
        deleteCudaMatrix2D(matrixB, n);
        deleteCudaMatrix2D(expected, m);
        deleteCudaMatrix2D(actual, m);
}