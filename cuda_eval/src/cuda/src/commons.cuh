#ifndef CUDA_EVAL_COMMONS_CUH
#define CUDA_EVAL_COMMONS_CUH

#include <string>
#include "types.hxx"

value_ptr createCudaMatrix(int, int);

value_ptr createCudaMatrixFromFile(const std::string &, int, int);

void deleteCudaMatrix(value_ptr);

value_ptr2D createCudaMatrix2D(int, int);

value_ptr2D createCudaMatrix2DFromFile(const std::string &, int, int);

void deleteCudaMatrix2D(value_ptr2D, int);

#endif //CUDA_EVAL_COMMONS_CUH
