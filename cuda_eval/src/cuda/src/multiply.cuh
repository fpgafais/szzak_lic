#ifndef CUDA_EVAL_MULTIPLY_CUH
#define CUDA_EVAL_MULTIPLY_CUH

#include "types.hxx"

__global__ void gpuMultiply(value_ptr, value_ptr, value_ptr, int, int, int);

__global__ void gpuMultiplySquare(value_ptr, value_ptr, value_ptr, int, int, int);

__global__ void gpuMultiply2D(value_ptr2D, value_ptr2D, value_ptr2D, int, int, int);

__global__ void gpuMultiplySquare2D(value_ptr2D, value_ptr2D, value_ptr2D, int, int, int);

#endif //CUDA_EVAL_MULTIPLY_CUH
