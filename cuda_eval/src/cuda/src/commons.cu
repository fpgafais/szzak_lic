#include "commons.hxx"
#include "commons.cuh"

value_ptr createCudaMatrix(int rowNum, int colNum) {
        value_ptr matrix;
        cudaMallocManaged(&matrix, rowNum * colNum * sizeof(value_t));
        fillMatrixWithZeros(matrix, rowNum, colNum);
        return matrix;
}

value_ptr createCudaMatrixFromFile(const std::string &name, int rowNum, int colNum) {
        auto matrix = createCudaMatrix(rowNum, colNum);
        loadMatrixFromFile(name, matrix, rowNum, colNum);
        return matrix;
}

void deleteCudaMatrix(value_ptr matrix) {
        cudaFree(matrix);
}

value_ptr2D createCudaMatrix2D(int rowNum, int colNum) {
        value_ptr2D matrix;
        cudaMallocManaged(&matrix, rowNum * sizeof(value_ptr));
        for (auto i = 0; i < rowNum; i++) {
                cudaMallocManaged(&(matrix[i]), colNum * sizeof(value_t));
        }
        fillMatrix2DWithZeros(matrix, rowNum, colNum);
        return matrix;
}

value_ptr2D createCudaMatrix2DFromFile(const std::string &name, int rowNum, int colNum) {
        auto matrix = createCudaMatrix2D(rowNum, colNum);
        loadMatrix2DFromFile(name, matrix, rowNum, colNum);
        return matrix;
}


void deleteCudaMatrix2D(value_ptr2D matrix, int rowNum) {
        for (auto i = 0; i < rowNum; i++) {
                cudaFree(matrix[i]);
        }
        cudaFree(matrix);
}
