#include "multiply.cuh"

__global__ void gpuMultiply(value_ptr matrixA, value_ptr matrixB, value_ptr outputMatrix, int m, int n, int w) {
        const int row = blockIdx.y * blockDim.y + threadIdx.y;
        const int col = blockIdx.x * blockDim.x + threadIdx.x;
        if (col < w && row < m) {
                const int inRow = row * n;
                const int outRow = row * w;
                int sum = 0;
                for (int i = 0; i < n; i++) {
                        sum += matrixA[inRow + i] * matrixB[i * w + col];
                }
                outputMatrix[outRow + col] = sum;
        }
}

__global__ void gpuMultiply2D(value_ptr2D matrixA, value_ptr2D matrixB, value_ptr2D outputMatrix, int m, int n, int w) {
        const int row = blockIdx.y * blockDim.y + threadIdx.y;
        const int col = blockIdx.x * blockDim.x + threadIdx.x;
        if (col < w && row < m) {
                int sum = 0;
                for (auto i = 0; i < n; i++) {
                        sum += matrixA[row][i] * matrixB[i][col];
                }
                outputMatrix[row][col] = sum;
        }
}
