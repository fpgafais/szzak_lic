#include <iostream>
#include <string>
#include <fstream>
#include <chrono>
#include "commons.hxx"

std::chrono::steady_clock::time_point getTime() {
        return std::chrono::steady_clock::now();
}

// common functions for 1D matrix impl

// CPU specific

void loadMatrixFromFile(const std::string &name, value_ptr matrix, int rowNum, int colNum) {
        std::ifstream inputFile(name);
        if (inputFile.is_open()) {
                for (auto i = 0; i < rowNum; i++) {
                        for (auto j = 0; j < colNum; j++) {
                                inputFile >> matrix[i * colNum + j];
                        }
                }
        } else throw;
}

value_ptr createMatrixFromFile(const std::string &name, int rowNum, int colNum) {
        auto matrix = new value_t[rowNum * colNum];
        loadMatrixFromFile(name, matrix, rowNum, colNum);
        return matrix;
}

// CPU specific END

void fillMatrixFromCin(value_ptr matrix, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        std::cin >> matrix[i * colNum + j];
                }
        }
}

value_ptr transposeMatrix(value_ptr matrix, int rowNum, int colNum) {
        value_ptr transposed = new value_t[colNum * rowNum];
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        transposed[i * colNum + j] = matrix[j * colNum + i];
                }
        }
        return transposed;
}

void fillMatrixWithZeros(value_ptr matrix, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        matrix[i * colNum + j] = 0;
                }
        }
}

void printMatrix(value_ptr matrix, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        std::cout << matrix[i * colNum + j] << " ";
                }
                std::cout << std::endl;
        }
}

void copyMatrix(value_ptr src, value_ptr dst, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        src[i * colNum + j] = dst[i * colNum + j];
                }
        }
}

// common functions for 2D matrix impl

// CPU specific

void loadMatrix2DFromFile(const std::string &name, value_ptr2D matrix, int rowNum, int colNum) {
        std::ifstream inputFile(name);
        if (inputFile.is_open()) {
                for (auto i = 0; i < rowNum; i++) {
                        for (auto j = 0; j < colNum; j++) {
                                inputFile >> matrix[i][j];
                        }
                }
        } else throw;
}

value_ptr2D createMatrix2DFromFile(const std::string &name, int rowNum, int colNum) {
        auto matrix = createMatrix2D(rowNum, colNum);
        loadMatrix2DFromFile(name, matrix, rowNum, colNum);
        return matrix;
}

value_ptr2D createMatrix2D(int rowNum, int colNum) {
        auto matrix = new value_ptr[rowNum];
        for (auto i = 0; i < rowNum; i++) {
                matrix[i] = new int[colNum];
        }
        fillMatrix2DWithZeros(matrix, rowNum, colNum);
        return matrix;
}

void deleteMatrix2D(value_ptr2D matrix2D, int rowNum) {
        for (auto i = 0; i < rowNum; i++) {
                delete[] matrix2D[i];
        }
        delete[] matrix2D;
}

// CPU specific END

void fillMatrix2DFromCin(value_ptr2D matrix2D, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        std::cin >> matrix2D[i][j];
                }
        }
}

void fillMatrix2DWithZeros(value_ptr2D matrix2D, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        matrix2D[i][j] = 0;
                }
        }
}

void printMatrix2D(value_ptr2D matrix2D, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        std::cout << matrix2D[i][j] << " ";
                }
                std::cout << std::endl;
        }
}

void copyMatrix2D(value_ptr2D src, value_ptr2D dst, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        src[i][j] = dst[i][j];
                }
        }
}

value_ptr2D transposeMatrix2D(value_ptr2D matrix, int rowNum, int colNum) {
        value_ptr2D transposed = createMatrix2D(rowNum, colNum);
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++) {
                        transposed[i][j] = matrix[j][i];
                }
        }
        return transposed;
}