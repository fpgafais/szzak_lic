#ifndef CUDA_EVAL_TYPES_HXX
#define CUDA_EVAL_TYPES_HXX

using value_t = int;
using value_ptr = value_t*;
using value_ptr2D = value_t**;

#include <chrono>

using time_unit = std::chrono::duration<double>;
constexpr char time_unit_s[] = " seconds";

#endif //CUDA_EVAL_TYPES_HXX
