#ifndef CUDA_EVAL_TEST_COMMONS_HXX
#define CUDA_EVAL_TEST_COMMONS_HXX

bool matrixEquals(value_ptr, value_ptr, int, int);

bool matrixEquals2D(value_ptr2D, value_ptr2D, int, int);

#endif //CUDA_EVAL_TEST_COMMONS_HXX
