#include "commons.hxx"
#include "test_commons.hxx"

bool matrixEquals(value_ptr firstMatrix, value_ptr secondMatrix, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++)
                        if (firstMatrix[i * colNum + j] != secondMatrix[i * colNum + j]) return false;
        }
        return true;
}

bool matrixEquals2D(value_ptr2D firstMatrix, value_ptr2D secondMatrix, int rowNum, int colNum) {
        for (auto i = 0; i < rowNum; i++) {
                for (auto j = 0; j < colNum; j++)
                        if (firstMatrix[i][j] != secondMatrix[i][j]) return false;
        }
        return true;
}