#ifndef CUDA_EVAL_COMMONS_HXX
#define CUDA_EVAL_COMMONS_HXX

#include <string>
#include <chrono>
#include "types.hxx"

std::chrono::steady_clock::time_point getTime();

// 1D matrix approach A[i * m + n]

// CPU only functions
void loadMatrixFromFile(const std::string &, value_ptr, int, int);

value_ptr createMatrixFromFile(const std::string &, int, int);
// CPU only functions END

void fillMatrixFromCin(value_ptr, int, int);

void fillMatrixWithZeros(value_ptr, int, int);

void printMatrix(value_ptr, int, int);

void copyMatrix(value_ptr, value_ptr, int, int);

value_ptr transposeMatrix(value_ptr, int, int);

// two dimensional matrix approach A[m][n]

// CPU only functions
value_ptr2D createMatrix2D(int, int);

void deleteMatrix2D(value_ptr2D, int);

void loadMatrix2DFromFile(const std::string &, value_ptr2D, int, int);

value_ptr2D createMatrix2DFromFile(const std::string &, int, int);
// CPU only functions END

void fillMatrix2DFromCin(value_ptr2D, int, int);

void fillMatrix2DWithZeros(value_ptr2D, int, int);

void printMatrix2D(value_ptr2D, int, int);

void copyMatrix2D(value_ptr2D, value_ptr2D, int, int);

value_ptr2D transposeMatrix2D(value_ptr2D, int, int);

#endif //CUDA_EVAL_COMMONS_HXX
